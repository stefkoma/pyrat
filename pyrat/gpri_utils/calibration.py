# -*- coding: utf-8 -*-
"""
Created on Thu May 15 14:56:18 2014

@author: baffelli
"""
import numpy as np

"""
Utilities for GPRI calibration
"""
import itertools as _itertools

import numpy as _np
from scipy import fftpack as _fftp, signal
from scipy import signal as _sig
from scipy import ndimage as _nd

from .. import core
from ..core import corefun
from ..fileutils import gpri_files as _gpf

import scipy.optimize as _opt

import matplotlib.pyplot as plt

import pyfftw.interfaces.scipy_fftpack as _fftp



import math as _math


def r_with_phase_center_shift(R_c, theta_c, theta, R_arm, R_ph):
    """
    This function computes the distance between the antenna
    phase center and a target located at `R_c`, `theta_c`, assuming
    the antenna has a phase center offset by `R_arm, R_ph` w.r.t to
    the angular positioner
    """
    R_ant = _np.sqrt(R_arm**2 + R_ph**2)
    alpha_ant = _np.arctan2(R_ph, R_arm)
    R = np.sqrt(R_c**2 + R_ant**2 - 2 * R_ant * R_c * _np.cos(theta + alpha_ant - theta_c))
    return R

def r_with_phase_center_shift_from_center(R_c, theta_c, theta, R_arm, R_ph):
    """
    This function computes the distance between the antenna
    phase center and a target located at `R_c`, `theta_c`, assuming
    the antenna has a phase center offset by `R_arm, R_ph` w.r.t to
    the angular positioner and the distance R_c is measured electronically
    relative to the antenna phase center
    """
    R_ant = _np.sqrt(R_arm**2 + R_ph**2)
    alpha_ant = _np.arctan2(R_ph, R_arm)
    R_tot = R_ant + R_c
    R = np.sqrt(R_tot**2 + R_ant**2 - 2 * R_ant * R_tot * _np.cos(theta + alpha_ant - theta_c))
    return R

def r_with_squinted_beam(R_c, theta_c, theta, R_arm, alpha_sq):
    """
    This function computes the distance between an antenna
    with a phase center located at `R_arm` and a point scatterer
    at `R_c`, `theta_c` as a function of the scan angle `theta`. Additionally,
    the antenna beam is squinted by `alpha_sq`, so that the mechanical
    and the electrical pointing directions do not coincide.
    """
    R = _np.sqrt(R_c**2 + R_arm**2 - 2 * R_arm * R_c * _np.cos(theta - alpha_sq - theta_c))
    return R

def r_old(R_c, theta_c, theta, R_arm, R_ph):
    return distance_from_phase_center(R_arm, R_ph, R_c, theta - theta_c, 1)[1]

def r_old_approx(R_c, theta_c, theta, R_arm, R_ph):
    return range_doppler_filter(R_arm, R_ph, R_c, theta - theta_c)




def distance_from_phase_center(L_arm, L_ph, R_0, theta, lam, wrap=False):
    """
    This function computes the relative phase caused by a shifted
    phase center in the antenna. The variation is computed relative to the slant of closest approach
    """
    L_ant = _np.sqrt(L_arm ** 2 + L_ph ** 2)
    alpha = _np.arctan2(L_ph , L_arm)
    # Chord length
    c = L_ant + R_0
    mixed_term = 2 * c * L_ant * _np.cos(theta + alpha)
    dist =  _np.sqrt(c ** 2 + L_ant ** 2 - mixed_term)
    rel_dist = R_0 - dist
    if wrap is True:
        return _np.mod(4 * _np.pi * rel_dist / lam, 2 * _np.pi), dist
    else:
        return (4 * _np.pi * rel_dist / lam), dist

def range_doppler_filter(L_arm, L_ph, R_0, theta):
    """
    This function computes a second order approximation of the distance
    between antenna and phase center, computed relative to the angle of closest approach
    """
    R_ant = _np.sqrt(L_arm ** 2 + L_ph ** 2)
    alpha = _np.arctan2(L_ph , L_arm)
    R_c =  R_0 + R_ant
    R = np.sqrt(R_c**2 + R_ant**2 - 2 * R_c * R_ant * np.cos(alpha))
    R_dot = (-np.sin(alpha) / R) * R_c * R_ant 
    R_ddot = (np.cos(alpha) / R) * R_c * R_ant - (np.sin(-alpha)**2 * R_c**2 * R_ant**2) / (R**3)
    rel_dist = R_dot * theta + 1/2 * R_ddot * theta**2 
    return rel_dist

def azimuth_filter(L_arm, L_ph, R_0, theta):
    r = r_with_phase_center_shift_from_center(R_0, 0, theta, L_arm, L_ph)
    r_0 = r_with_phase_center_shift_from_center(R_0, 0, 0, L_arm, L_ph)
    return r - r_0

def measure_phase_center_location(slc, ridx, azidx, sw=(2,10), aw=90, unwrap=True, distance_function=r_with_phase_center_shift_from_center):

    #lever arm length is given by antenna phase center x position
    """
        Determine the antenna phase center shift according to the lever arm model
    Parameters
    ----------
    slc : fleutils.gammaDataset
        the gammaDataset object to analyze
    ridx : float
        range position of point target to analyzed
    azidx : float
        azimuth position of point target
    sw  : iterable
        search window in azimuth and range to determine the exact position
    aw  : float
        analysis window
    distance_function  : function
        must return the distance between target and phase center,

    unwrap

    Returns
    -------

    """
    r_arm = 0.25
    lam = _gpf.C  /slc.radar_frequency  

    def cf(R_c, theta_c, theta_vec, meas_phase, off, params):
        sim_dist = distance_function(R_c, theta_c, theta_vec, *params)
        sim_phase = (4 * np.pi * (R_c - sim_dist) / lam) + off
        cost = _np.sum(_np.abs(sim_phase - meas_phase) ** 2)
        return cost
    # Find the maxium in search window
    max_r, max_az = corefun.maximum_around(_np.abs(slc), [ridx, azidx], sw)
    #Determine analysis window
    max_slice = corefun.window_idx(slc, (max_r, max_az), (1, aw))
    reflector_slice = slc[max_slice]
    max_pwr = _np.abs(slc[max_r, max_az])**2
    ref_pwr = _np.abs(reflector_slice)**2 
    # Determine half power beamwidth
    half_pwr_idx = _np.nonzero(ref_pwr >= max_pwr * 0.45)
    # Slice slc
    reflector_slice = reflector_slice[half_pwr_idx]
    # Determine parameters
    R_c = reflector_slice.r_vec
    theta = reflector_slice.az_vec
    theta_c = reflector_slice.az_vec[reflector_slice.shape[0] // 2]
    refl_ph = _np.angle(reflector_slice)
    if unwrap:
        refl_ph = _np.unwrap(refl_ph)
    else:
        refl_ph = refl_ph
    #reference the phase
    refl_ph -= refl_ph[refl_ph.shape[0] // 2]
    # Define cost function
    cost_VV = lambda par_vec: cf(R_c, _np.deg2rad(theta_c), _np.deg2rad(theta), refl_ph, par_vec[0],[r_arm, par_vec[1]])
    # Solve optimization problem
    res = _opt.minimize(cost_VV, [0,0], bounds=((None,None), (-2, 2)), method="L-BFGS-B")
    sim_dist = distance_function(R_c,  _np.deg2rad(theta_c), _np.deg2rad(theta), r_arm, res.x[1::])
    sim_ph = 4 * np.pi/lam * (R_c - sim_dist) + res.x[0]
    print(res)
    par_dict = {'phase_center_offset': [res.x[0], 'm'], 'residual error': res.fun,
                'lever_arm_length': [r_arm, 'm'], 'range_of_closest_approach': [R_c, 'm']}
    # #Compute RVP
    rel_dist = R_c - sim_dist
    rvp = _np.exp(1j * 4 * rel_dist**2 * slc.chirp_bandwidth / _gpf.C**2)
    # #Compute rcm
    rcm = _np.max(rel_dist)
    return res.x[1], res.fun, R_c, refl_ph, sim_ph, rvp, rcm


def range_resolution(B):
    return _gpf.C / (2 * B)

def calibrate_s_from_r_and_t(S, R, T):
    #Compute distortion matrix M
    M = np.kron(R,T)
    M_inv = np.linalg.inv(M)
    #Vectorize scattering matrix
    S_vec = corefun.vectorize_matrix(S)
    #Append an extra dim to make product with @ work
    #Apply calibration
    S_cal = (M_inv @ _np.expand_dims(S_vec,-1)).reshape(S.shape)
    return S_cal

def cr_rcs(l, freq, type='triangular'):
    lam = _gpf.lam(freq)
    if type == 'triangular':
        RCS  = 4 / 3 * _np.pi * l**4 / (lam**2)
    elif type == 'cubic':
        RCS = 12 * _np.pi * l**4 / (lam**2)
    return RCS


def remove_window(S):
    spectrum = _np.mean(_np.abs((_fftp.fft(S, axis=1))), axis=0)
    spectrum = corefun.smooth(spectrum, 5)
    spectrum[spectrum < 1e-6] = 1
    S_f = _fftp.fft(S, axis=1)
    S_corr = _fftp.ifft(S_f / spectrum, axis=1)
    return S_corr



def symmetric_pad(pad_size):
    # if pad_size % 2 == 0:
    #     left_pad = right_pad = np.ceil(-pad_size//2)  + 1
    # else:
    left_pad = int(np.floor(pad_size/2)) + 1
    right_pad = int(np.ceil(pad_size/2))
    pad_vec = (left_pad, right_pad)
    return pad_vec

def filter2d(slc, filter):
    filter_pad_size = ((0,0), symmetric_pad(slc.shape[1]+filter.shape[1]))
    filter_pad = np.pad(filter, filter_pad_size, mode='constant')
    slc_pad_size = ((0,0,), symmetric_pad(filter.shape[1]*2))
    slc_pad = np.pad(slc,slc_pad_size, mode='constant')
    #Transform
    fft_fun = lambda arr: _fftp.fft(arr, axis=1)
    ifft_funt = lambda arr: _fftp.ifftshift(_fftp.ifft(arr, axis=1),axes=(1,))
    filter_hat = fft_fun(filter_pad)
    slc_hat = fft_fun(slc_pad)
    #Product and invese transform
    slc_filt = ifft_funt(slc_hat * filter_hat)
    slc_filt = slc_filt[:, slice(slc_pad_size[1][0],slc_filt.shape[1]-slc_pad_size[1][1])]
    return slc_filt

def filter1d(slc, filt):
    cpx_cov = lambda img, filt: _sig.convolve(img, filt, mode='same')
    padding = filt.shape[1]
    slc_filt = np.zeros_like(slc)
    for idx_row in range(slc.shape[0]):
        if idx_row % 1000 == 0:
            print('Processing range index: ' + str(idx_row))
        slc_pad = slc[idx_row,:]
        slc_filt[idx_row,:] = cpx_cov(slc_pad, filt[idx_row,:]).astype(slc.dtype)
    return slc_filt


def azimuth_correction(slc, r_ph, ws=0.6, discard_samples=False, filter_fun=filter1d, ref_fun=azimuth_filter):
    r_arm = slc.phase_center[0]
    ws_samp = int(ws / slc.GPRI_az_angle_step)
    theta = _np.arange(-ws_samp // 2, ws_samp // 2) * _np.deg2rad(np.abs(slc.GPRI_az_angle_step))
    rr, tt = np.meshgrid(slc.r_vec, theta, indexing='ij') 
    lam = _gpf.C / slc.radar_frequency
    #Compute filter
    filt2d  = np.pi * 4 / lam * ref_fun(r_arm, r_ph, rr, tt)
    matched_filter2d = (_np.exp(-1j*filt2d))
    #Normalize filter
    matched_filter2d /= _np.sum(_np.abs(filt2d),axis=1)[:,None]
    #Apply filter
    slc_filt_2d = filter_fun(slc.astype(_np.complex64), matched_filter2d)
    slc_filt = slc.__array_wrap__(slc_filt_2d).astype(slc.dtype)
    if discard_samples:
        slc_filt = slc_filt.decimate(ws_samp, mode='discard')
    return slc_filt

def time_domain_processor(slc, r_ph, ws=0.4):
    lam = _gpf.C / slc.radar_frequency
    slc_comp = np.zeros(slc.shape, dtype=np.complex64)
    r_arm = slc.phase_center[0]
    ws_samp = int(ws / slc.GPRI_az_angle_step)
    for r_idx in range(slc.shape[0]):
        R_c = slc.r_vec[r_idx]
        for az_idx in range(slc.shape[1]):
            theta_c = _np.deg2rad(slc.az_vec[az_idx])
            theta_win = corefun.window_idx(slc, (r_idx,az_idx), (1, ws_samp))
            samp_to_sum = slc[theta_win]
            theta_vec = np.deg2rad(samp_to_sum.az_vec)
            rel_dist = R_c - r_with_phase_center_shift_from_center(R_c,theta_vec, theta_c,r_arm, r_ph) 
            filt = np.exp(4*_np.pi*1j * rel_dist/lam)
            slc_comp[r_idx, az_idx] = np.mean(samp_to_sum.squeeze() * filt.conj())
    return slc.__array_wrap__(slc_comp).astype(slc.dtype)

def measure_imbalance(C_tri, C):
    """
    This function measures the co and crosspolar imbalance
    Parameters
    ----------
    C_tri   : coherencyMatrix
        the covariance matrix of a calibration trihedral
    C   : coherencyMatrix
    the covariance to estimate the HV VH imbalanace
    Returns
    -------

    """
    if C_tri.shape[-1] == 3:
        idx_H = 0
        idx_V = 2
    else:
        idx_H = 0
        idx_V = 3
    assert C.shape[-1] == 4
    f = (_np.abs(C_tri[idx_V, idx_V]) / _np.abs(C_tri[idx_H, idx_H])) ** (1 / 4.0)
    VV_HH_phase_bias = _np.angle(C_tri[idx_V, idx_H])
    g = (_np.mean(np.abs((C[:, :, 1, 1]))) / _np.mean(np.abs(C[:, :, 2, 2]))) ** (1 / 4.0)
    cross_pol_bias = _np.nanmean(_np.angle(C[:, :, 1, 2]))
    # Solve for phi t and phi r
    phi_t = (VV_HH_phase_bias + cross_pol_bias) / 2
    phi_r = (VV_HH_phase_bias - cross_pol_bias) / 2
    return phi_t, phi_r, f, g


def gpri_radcal(mli, tri_pos, sigma, dB=False):
    """
    Compute the GPRI radiometric calibration parameter
    Parameters
    ----------
    mli : gammaDataset
        the MLI image to use to determine the calibration constant
    tri_pos : iterable
        the location of the calibration area
    sigma : float the RCS 
    db : bool
        if set to true, use the RCS in dB and return the factor in dB


    Returns
    -------

    """
    # extract the point target response to get the maximum
    mli_ptarg, rplot, azplot, mx_idx, res_dic, r_vec, az_vec = corefun.ptarg(mli, tri_pos[0], tri_pos[1], sw=(2,2), rwin=5, azwin=5)
    # illuminated area
    A_illum = mli.range_pixel_spacing *_np.deg2rad(_gpf.AZ_BW) * mli.r_vec[tri_pos[0]]
    # Calibration factor
    if dB:
        sigma_lin = 10**(10/sigma)
    else:
        sigma_lin = sigma
    K = sigma_lin/(mli_ptarg[mx_idx] * A_illum)
    if dB:
        K_out = 10 * np.log10(float(K))
    else:
        K_out = K
    return K_out


def distortion_matrix(phi_t, phi_r, f, g):
    dm = _np.diag([1, f * g * _np.exp(1j * phi_t), f / g * _np.exp(1j * phi_r),
                                  f ** 2 * _np.exp(1j * (phi_r + phi_t))])
    return dm

def r_and_t_matrices(phi_t, phi_r, f, g):
    R = np.array([[1,0],[0,f/g*np.exp(1j*phi_r)]])
    T = np.array([[1,0],[0,f*g*np.exp(1j*phi_t)]])
    return R,T

def apply_polcal_to_s(R,S,T):
    S_cal = (np.linalg.pinv(R) @ S @ np.linalg.pinv(T))
    return S_cal

def calibrate_from_parameters(S, par):
    # TODO check calibration
    """
    This function performs polarimetric calibration from a text file containing the m-matrix distortion parameters
    Parameters
    ----------
    S : scatteringMatrix, coherencyMatrix
        the matrix to calibrate
    par : string, array_like
        Path to the parameters file, it must in the form of _np.save
    """
    if isinstance(par, str):
        m = _np.fromfile(par)
        m = m[::2] + m[1::2]
    elif isinstance(par, (_np.ndarray, list, tuple)):
        m = par
    else:
        raise TypeError("The parameters must be passed\
        as a list or array or tuple or either as a path to a text file")
    if isinstance(S, core.matrices.scatteringMatrix):
        f = m[0]
        g = m[1]
        S_cal = S.__copy__()
        S_cal['VV'] = S_cal['VV'] * 1 / f ** 2
        S_cal['HV'] = S_cal['HV'] * 1 / f * 1 / g
        S_cal['VH'] = S_cal['VH'] * 1 / f
        S_cal = S.__array_wrap__(S_cal)
    return S_cal


def scattering_matrix_to_flat_covariance(S, flat_ifgram, B_if):
    C = S.to_coherency_matrix(basis='lexicographic', bistatic=True)
    # Convert mapping into list of tuples
    mapping_list = [(key, value) for key, value in _gpf.channel_dict.items()]
    for (name_chan_1, idx_chan_1), (name_chan_2, idx_chan_2) in _itertools.product(mapping_list, mapping_list):
        # convert the indices of the two channels into the indices to access the covariance component
        idx_c_1 = _np.ravel_multi_index(idx_chan_1, (2, 2))
        idx_c_2 = _np.ravel_multi_index(idx_chan_2, (2, 2))
        pol_baseline = S.phase_center_array[name_chan_1][-1] - S.phase_center_array[name_chan_2][-1]
        ratio = pol_baseline / float(B_if)
        C[:, :, idx_c_1, idx_c_2] *= _np.exp(-1j * flat_ifgram * ratio)
    return C


def coregister_channels(S):
    """
    This function coregisters the GPRI channels by shifting each channel by the corresponding number of samples in azimuth
    It assumes the standard quadpol AAA-ABB-BAA-BBB TX-RX-seq.
    
    Parameters
    ----------
    S : pyrat.core.matrices.scatteringMatrix
        The scattering matrix to be coregistered.
    
    Returns
    -------
    scatteringMatrix
        The image after coregistration.
    """
    S1 = S
    S1['VV'] = corefun.shift_array(S['VV'], (3, 0))
    S1['HV'] = corefun.shift_array(S['HV'], (1, 0))
    S1['VH'] = corefun.shift_array(S['VH'], (2, 0))
    return S1



def remove_window(S):
    spectrum = _np.mean(_np.abs(_fftp.fftshift(_fftp.fft(S, axis=1), axes=(1,))), axis=0)
    spectrum = corefun.smooth(spectrum, 5)
    spectrum[spectrum < 1e-6] = 1
    S_f = _fftp.fft(S, axis=1)
    S_corr = _fftp.ifft(S_f / _fftp.fftshift(spectrum), axis=1)
    return S_corr


def synthetic_interferogram(S, DEM, B):
    """
    Parameters
    ----------
    S : scatteringMatrixc
        The image to correct
    DEM : ndarray
        A DEM in the same coordinates as the image
    """

    # Coordinate System W.R.T antenna
    r, th = _np.meshgrid(S.r_vec, S.az_vec)
    x = r * _np.cos(th)
    y = r * _np.sin(th)
    z = DEM
    r1 = _np.dstack((x, y, z))
    r2 = _np.dstack((x, y, (z + B)))
    # Convert into
    delta_d = _np.linalg.norm(r1, axis=2) - _np.linalg.norm(r2, axis=2)
    lamb = 3e8 / S.center_frequency
    ph = delta_d * 4 / lamb * _np.pi
    return _np.exp(1j * ph)



def squint_vec(rawdata, z=2500):
    win_slice = slice(z, rawdata.shape[0] - z)
    rawdata_sl = rawdata[win_slice,:]#window the edges
    max_idx = np.argmax(np.abs(rawdata_sl), axis=1)
    return max_idx, win_slice


def fit_squint(raw, slc_par, azidx, ridx, win=(10,10), z=2500):
    """
    Performs a fit of squint-angle verus chirp frequency on the raw data
    by analyzing the response of a point-like target
    Parameters
    ----------
    raw : pyrat.fileutils.rawData
    slc_par : pyrat.fileutils.ParameterFile
    azidx :  int
        index of point target in azimuth
    ridx : int
        index of point target in range
    win : tuple
        window to extract around the point target
    z : integer
        number to samples to discard and the beginning and end of each chirp

    Returns
    -------

    """
    az_slice = raw.azimuth_slice_from_slc_idx(azidx, win[1])
    # Construct
    raw_sl = raw[:, az_slice] * 1
    # Range filter
    fshift = np.ones(raw_sl.shape[0])
    fshift[1::2] = -1
    raw_filt = _sig.hilbert(raw_sl.filter_range_spectrum(slc_par, ridx, win[0], k=2), axis=0)
    #Reference phase
    az_vec = np.arange(-raw_filt.shape[1] // 2, raw_filt.shape[1] // 2) * raw.azspacing
    # Find maximum
    squint_idx, win_slice = squint_vec(raw_filt, z=z)
    #Find phase at maximum
    ref_ph = np.angle([raw_filt[z:,:][rg, i] for (rg,i) in enumerate(squint_idx)])
    #Reference filtered
    raw_filt = np.exp(-1j*ref_ph)[:,None] * raw_filt[z*2:,:]
    #Extract squint phase 
    squint_phase = np.unwrap(np.angle([raw_filt[rg, i] for rg, i in enumerate(squint_idx)]))
    squint_ang = az_vec[squint_idx[::-1]]
    # fit squint
    freqvec_fit = raw.freqvec[win_slice]
    az_vec_fit = az_vec
    sq_par = np.polyfit(freqvec_fit, squint_ang, 1)
    #fit phase
    sq_ph_par = np.polyfit(freqvec_fit, squint_phase, 1)
    return squint_idx, az_vec_fit, squint_ang, sq_par, squint_phase, sq_ph_par, raw_filt, freqvec_fit