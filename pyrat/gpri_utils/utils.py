
import numpy as _np
from ..fileutils import gpri_files as gpf

def r_vec(gpri_par, spacing='data'):
    nr = gpf.get_width(gpri_par)
    if spacing == 'data':
        r = gpri_par.near_range_slc + gpri_par.range_pixel_spacing * _np.arange(nr)
    else:
         r = _np.linspace(gpri_par.near_range_slc, gpri_par.range_pixel_spacing * nr, spacing)
    return r

def az_vec(gpri_par, spacing='data'):
    na = gpf.get_nlines(gpri_par)
    if spacing == 'data':
        a = gpri_par.GPRI_az_start_angle + gpri_par.GPRI_az_angle_step * _np.arange(na)
    else:
        a = _np.linspace(gpri_par.GPRI_az_start_angle, gpri_par.GPRI_az_angle_step * na, spacing)
    return a

def rtoi(r, gpri_par):
    return (r -  gpri_par.near_range_slc) // gpri_par.range_pixel_spacing

def atoi(a, gpri_par):
    return (a -  gpri_par.GPRI_az_start_angle) // gpri_par.GPRI_az_angle_step
