#!/usr/bin/env python3
import argparse
import pyrat.fileutils.gpri_files as gpf
import numpy as np
import matplotlib.pyplot as plt

import pyrat.core.corefun as cf
import pyrat.core.polfun as pf
import pyrat.visualization.visfun as vf

import scipy as sp
import scipy.spatial as spat

import pyrat.core.matrices as mat

import pyrat.gpri_utils.calibration as cal

parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)


parser.add_argument("channels", nargs=4, type=str, help="HH, HV, VH, VV SLC")
parser.add_argument("channel_pars",nargs=4 ,type=str, help="HH, HV, VH, VV calibrated slc")
parser.add_argument("cal_par",type=str, help="Path of calibration parameters file")
parser.add_argument("out_root",type=str, help="Path of calibration parameters file")
args = parser.parse_args()


chans = []
pars = []
for slc,slc_par in zip(args.channels, args.channel_pars):
    master_slc = gpf.gammaDataset.fromfile(slc_par,slc)
    chans.append(master_slc)
    pars.append(gpf.par_to_dict(slc_par))


S_matrix = np.dstack(chans).reshape(chans[0].shape[0:2]+ (2,2)).view(mat.scatteringMatrix)

#Load calibration parameters
cal_par = gpf.par_to_dict(args.cal_par)
#Solve for phi_r and phi_t
f = cal_par['f']
g = cal_par['g']
phi_r = cal_par['phi_r']
phi_t = cal_par['phi_t']
K = cal_par['calibration_gain']


R,T  = cal.r_and_t_matrices(phi_t, phi_r, f, g)
S_cal = cal.calibrate_s_from_r_and_t(S_matrix, R,T)

S_cal = 10**(K/20) * mat.scatteringMatrix.frommatrix(S_cal)


for chan, par in zip(['HH','HV','VH','VV'], pars):
    out_file_root = args.out_root + "_" + chan
    gpf.write_binary(S_cal[chan],  out_file_root + ".slc")
    gpf.dict_to_par(par,  out_file_root + ".slc.par")

