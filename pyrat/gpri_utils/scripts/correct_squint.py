#!/usr/bin/python
__author__ = 'baffelli'

import argparse
import shutil as _shutil
import sys

import numpy as _np
import pyrat.fileutils.gpri_files as gpf





def main():
    # Read the arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('raw',
                        help="GPRI raw file")
    parser.add_argument('raw_par',
                        help="GPRI raw file parameters")
    parser.add_argument('raw_out', type=str,
                        help="Corrected GPRI raw file")
    parser.add_argument('raw_par_out', type=str,
                        help="Parameters of the corrected GPRI raw file")
    parser.add_argument('sq_rate', help='Squint rate in deg/GHz', type=float)

    # Read arguments
    try:
        args = parser.parse_args()
    except:
        print(parser.print_help())
        sys.exit(-1)

    # Load data
    raw = gpf.rawData(args.raw_par, args.raw)
    #Correct
    rawDesq = gpf.correct_squint(raw, squint_rate=args.sq_rate*1e-9)
    #Save
    rawDesq.tofile(args.raw_par_out, args.raw_out)

if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        pass
