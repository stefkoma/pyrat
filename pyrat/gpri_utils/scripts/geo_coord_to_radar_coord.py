#!/usr/bin/env python3
import pyrat.geo.geofun as gf
import numpy as np
import itertools
import csv
import argparse as ap
parser = ap.ArgumentParser()
parser.add_argument("dem_seg_par", type=str)
parser.add_argument("lut", type=str)
parser.add_argument("ref_mli_par", type=str)
parser.add_argument("inverse_lut", type=str)
parser.add_argument("coord", nargs=2, type=float)
args = parser.parse_args()
map = gf.GeocodingTable(args.dem_seg_par, args.lut, args.ref_mli_par, args.inverse_lut)
center_radar = map.geo_coord_to_radar_coord(args.coord)
print("{} {}".format(*center_radar))