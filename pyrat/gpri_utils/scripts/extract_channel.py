#!/usr/bin/python3
__author__ = 'baffelli'


import argparse
import os
import sys

import pyrat.fileutils.gpri_files as gpf


#


def main():
    # Read the arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('raw',
                        help="GPRI raw file")
    parser.add_argument('raw_par',
                        help="GPRI raw file parameters")
    parser.add_argument('raw_out', type=str,
                        help="GPRI raw file corresponding to the extracted channel")
    parser.add_argument('raw_par_out', type=str,
                        help="GPRI raw parameter file corresponding to the extracted channel")
    parser.add_argument('pat', type=str,
                        help="Pattern to extract")
    # Read arguments
    try:
        args = parser.parse_args()
    except:
        print(parser.print_help())
        sys.exit(-1)
    raw = gpf.rawData(args.raw_par, args.raw)
    raw_chan = raw.extract_channel(args.pat[0:3], args.pat[-1])
    raw_chan.tofile(args.raw_par_out, args.raw_out)

if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        pass
