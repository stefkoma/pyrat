
#!/usr/bin/env python3
import argparse
import pyrat.fileutils.gpri_files as gpf
import numpy as np
import matplotlib.pyplot as plt

import pyrat.core.corefun as cf
import pyrat.core.polfun as pf
import pyrat.visualization.visfun as vf

import scipy as sp
import scipy.spatial as spat

import pyrat.core.matrices as mat

import pyrat.gpri_utils.calibration as cal

import pyrat.fileutils.parameters as params

parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)


parser.add_argument("slc", type=str, help="SLC data")
parser.add_argument("slc_par",type=str, help="SLC parameters")
parser.add_argument("slc_cal",type=str, help="Radiometrically calibrated SLC")
parser.add_argument("slc_cal_par",type=str, help="Radiometrically calibrated SLC parameters")
parser.add_argument("sf",type=float, help="Scale factor in dB")
parser.add_argument("--pix_area",type=str, help="Pixel area")

args = parser.parse_args()

#Load data
slc = gpf.gammaDataset.fromfile(args.slc_par, args.slc, )
#Apply incidence angle correction
if args.pix_area:   
    pix_area = gpf.load_binary(args.inc, slc.shape[0], dtype=gpf.FLOAT)
    slc_norm = slc / pix_area
else:
    slc_norm = slc

slc_cal = slc_norm * 10**(args.sf/20)

slc_cal.tofile(args.slc_cal_par, args.slc_cal)

