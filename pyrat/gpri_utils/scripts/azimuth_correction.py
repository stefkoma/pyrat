#!/usr/bin/python
__author__ = 'baffelli'

import argparse
import sys

import numpy as _np
import pyrat.fileutils.gpri_files as _gpf
import pyrat.gpri_utils.calibration as _cal
import scipy.signal as _sig



def main():
    # Read the arguments
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('slc',
                        help="Uncorrected slc file")
    parser.add_argument('slc_par',
                        help="Slc parameters")
    parser.add_argument('slc_out', type=str,
                        help="Output slc")
    parser.add_argument('slc_par_out', type=str,
                        help="Output slc parameters")
    parser.add_argument('--r_ant', type=float, default=argparse.SUPPRESS,
                        help="Antenna rotation arm length. If not specified, it is calculated from the GPRI parameter file")
    parser.add_argument('--r_ph',
                        help="Antenna phase center horizontal displacement",
                        type=float, default=0.15)
    parser.add_argument('-w', '--win_size', dest='ws',
                        help="Convolution window size in degrees",
                        type=float, default=0.4)
    parser.add_argument('-d', '--discard-samples', dest='discard_samples',
                        help="Discard samples after convolution",
                        action='store_true')
    parser.add_argument("-a",help="Approximate reference function", action="store_true", default=False)
    # Read arguments
    try:
        args = parser.parse_args()
    except:
        print(parser.print_help())
        sys.exit(-1)
    ref_fun = _cal.range_doppler_filter if args.a else _cal.azimuth_filter
    slc = _gpf.gammaDataset.fromfile(args.slc_par, args.slc)
    slc_corr = _cal.azimuth_correction(slc, args.r_ph, ws=args.ws, discard_samples=args.discard_samples, ref_fun=ref_fun)
    slc_corr.tofile(args.slc_par_out, args.slc_out)

if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        pass
