#!/usr/bin/python
__author__ = 'baffelli'

import argparse
import sys

import pyrat.core.corefun as cf
import pyrat.fileutils.gpri_files as _gpf


def main():
    # Read the arguments
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('slc', type=str,
                        help="SLC channel file")
    parser.add_argument('slc_par', type=str,
                        help="SLC channel file parameters")
    parser.add_argument('slc_out',
                        help="Decimated slc", type=str)
    parser.add_argument('slc_par_out', type=str,
                        help="Output slc parameters")
    parser.add_argument('dec', type=int,
                        help="Slc decimation factor")
    parser.add_argument('mode', type=int, default=0,
                        help='Decimation mode: 0 (default) adding  n samples, 1 discarding every nth-sample')
    # Read arguments
    try:
        args = parser.parse_args()
    except:
        print(parser.print_help())
        sys.exit(-1)
    # Load slc
    slc = _gpf.gammaDataset(args.slc_par, args.slc)
    # Decimate
    mode_mapping = {0: 'sum', 1: 'discard'}
    slc_dec = slc.decimate(args.dec, mode=mode_mapping[args.mode])
    slc_dec.tofile(args.slc_par_out, args.slc_out)


if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        pass
