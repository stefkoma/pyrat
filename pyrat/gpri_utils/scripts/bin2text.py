#!/usr/bin/env python3
import pyrat.fileutils.gpri_files as gpf
import numpy as np
import itertools
import csv
import argparse as ap
import pandas as pd

parser = ap.ArgumentParser(description="Convert binary to csv table")
parser.add_argument("dem_seg_par", type=str, help="Dem seg file")
parser.add_argument("bin_csv", type=str, help="Output, csv file of range index, azimuth index, data")
parser.add_argument("--bin", type=str, help="Binary files,", nargs="*")
parser.add_argument("--names", type=str, help="Filenames, used as header,", nargs="*")
args = parser.parse_args()


bins = [ gpf.gammaDataset.fromfile(args.dem_seg_par, d) for d in args.bin]
rr, aa = np.mgrid[0:bins[0].shape[0], 0:bins[0].shape[1]]

all_bins = [rr, aa] + bins
all_names = ["ridx", "azidx"] + args.names

records = [(nm, bn.flatten()) for nm, bn in zip(all_names, all_bins)]


#Generate dataframe
coord_data = pd.DataFrame.from_items(records)
coord_data.to_csv(args.bin_csv, index = False)