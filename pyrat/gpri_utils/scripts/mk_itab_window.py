#!/usr/bin/env python3
import argparse
import numpy as np
import csv
import pyrat.fileutils.gpri_files as gpf
import matplotlib.pyplot as plt
import pyrat.core.corefun as cf
import pyrat.diff.utils as dut
import datetime as dt

parser = argparse.ArgumentParser(description="Create an itab file")
parser.add_argument("itab", help="(Output)Tab separated file with 4 columns: (1) Ref SLC index, (2) Second SLC index, (3) Line Counter, (4) Active flag", type=str)
parser.add_argument("max", help="Number of slcs", type=int)
parser.add_argument("step", help="Difference between reference and second slc", type=int)
parser.add_argument("stride", help="Increment of the first slc", type=int)
parser.add_argument("start", help="Starting index of first slc", type=int)
parser.add_argument("-r", help="Reference slc if stride is 0", default=1)
parser.add_argument("--df", help="Date format", default="%Y%m%d_%H%M%S")
parser.add_argument("--max_distance", help="Maximum distance in indices between master and slave", default=20, type=int)

try:
    args = parser.parse_args()
except:
    parser.print_help()
    exit()




itable = dut.Itab.fromparameters(args.max, stride=args.stride,step=args.step, max_distance=args.max_distance) 


#Now compute all baselines
itable.tofile(args.itab)

