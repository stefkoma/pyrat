
#!/usr/bin/env python3
import argparse
import pyrat.fileutils.gpri_files as gpf
import numpy as np
import matplotlib.pyplot as plt

import pyrat.core.corefun as cf
import pyrat.core.polfun as pf
import pyrat.visualization.visfun as vf

import scipy as sp
import scipy.spatial as spat

import pyrat.core.matrices as mat

import pyrat.gpri_utils.calibration as cal

import pyrat.fileutils.parameters as params

parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)


parser.add_argument("channels", nargs=4, type=str, help="HH, HV, VH, VV SLC")
parser.add_argument("channel_pars",nargs=4 ,type=str, help="HH, HV, VH, VV calibrated slc")
parser.add_argument("output_par",type=str, help="Output path of the calibration parameters")
parser.add_argument("tcr_coord",help="Coordinates of calibration point", type=int, nargs=2)
parser.add_argument("--rcs",help="Optional rcs in dB", type=float)
args = parser.parse_args()


chans = []
for slc,slc_par in zip(args.channels, args.channel_pars):
    master_slc = gpf.gammaDataset.fromfile(slc_par,slc)
    chans.append(master_slc)


S_matrix = mat.scatteringMatrix.frommatrix(np.dstack(chans).reshape(chans[0].shape[0:2]+ (2,2)), params=chans[0]._params)
C_matrix = S_matrix.to_coherency_matrix(basis='lexicographic', bistatic=True)


assert C_matrix.basis == 'lexicographic'
assert C_matrix.shape[-1] == 4
C_matrix_av = C_matrix.boxcar_filter([5,2])
assert C_matrix_av.basis == 'lexicographic'
assert C_matrix.shape[-1] == 4
#Zoom 
ridx = args.tcr_coord[0]
azidx = args.tcr_coord[1]
mx_idx = cf.maximum_around(np.abs(C_matrix[:,:,0,0]+C_matrix[:,:,3,3]), (ridx,azidx), (5,5))
phi_t, phi_r, f, g = cal.measure_imbalance(C_matrix[mx_idx[0], mx_idx[1],:,:], C_matrix_av)

if args.rcs:
    K = cal.gpri_radcal(np.abs(S_matrix['HH'])**2, [ridx, azidx], args.rcs, dB=True)
else:
    K = 0
g = np.abs(g)
f = np.abs(f)
cal_dict = dict(g={"value":g},f={"value":f},phi_r={"value":phi_r}, phi_t={"value":phi_t},calibration_gain={"value":K, "unit":'dB'})

new_par = params.ParameterFile.from_dict(cal_dict)

new_par.tofile(args.output_par)
