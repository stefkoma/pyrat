#!/usr/bin/python
__author__ = 'baffelli'

import argparse
import sys

import numpy as _np
import pyrat.fileutils.gpri_files as _gpf


def main():
    # Read the arguments
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    # subparsers = parser.add_subparsers()
    parser.add_argument('raw',
                        help="Raw channel file")
    parser.add_argument('raw_par',
                        help="GPRI raw file parameters")
    parser.add_argument('slc_out', type=str,
                        help="Output slc")
    parser.add_argument('slc_par_out', type=str,
                        help="Slc parameter file")
    parser.add_argument('-d', type=int, default=5,
                        help="Decimation factor")
    parser.add_argument('-z',
                        help="Number of samples to zero at the beginning of each pulse", dest='zero',
                        type=int, default=300)
    parser.add_argument("-k", type=float, default=3.00, dest='kbeta',
                        help="Kaiser Window beta parameter")
    parser.add_argument("-s", "--apply_scale", default=True, dest='apply_scale', action='store_false')
    parser.add_argument('-r', help='Near range for the slc', dest='rmin', type=float, default=50)
    parser.add_argument('-R', help='Far range for the slc', dest='rmax', type=float, default=1000)
    parser.add_argument('--rvp_corr', help='Correct range video phase', dest='rvp_corr', default=False,
                        action='store_true')
    parser.add_argument("--remove_offset", default=False,
                        action='store_true')
    # Read adrguments
    try:
        args = parser.parse_args()
    except:
        print(parser.print_help())
        sys.exit(-1)
    #Load raw data
    raw = _gpf.rawData(args.raw_par, args.raw)
    slc = _gpf.range_compression(raw, rmin=args.rmin, \
    rmax=args.rmax, dec=args.d, kbeta=args.kbeta, zero=args.zero, rvp_corr=False, remove_offset=args.remove_offset)
    #Apply range compression
    slc.tofile(args.slc_par_out, args.slc_out)



if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        pass
