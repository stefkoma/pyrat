#!/usr/bin/env python3
import pyrat.fileutils.gpri_files as gpf
import numpy as np
import itertools
import csv
import argparse as ap
import pandas as pd

parser = ap.ArgumentParser(description="Convert matrices containing coordinates into a table")
parser.add_argument("dem_seg_par", type=str)
parser.add_argument("east", type=str, help="easting coordinates, same size as DEM,")
parser.add_argument("north", type=str, help="northing coordinates, same size as DEM,")
parser.add_argument("coord", type=str, help="Output, csv file of range index, azimuth index, easting, northing")
args = parser.parse_args()
east = gpf.gammaDataset.fromfile(args.dem_seg_par, args.east)
north = gpf.gammaDataset.fromfile(args.dem_seg_par, args.north)

rr, ii = np.mgrid[0:east.shape[0], 0:east.shape[1]]

records = list(map(lambda x: (x[0], x[1].flatten()), [("east", east), ("north", north), ("ridx", rr), ("azidx", ii)]))
#Generate dataframe
coord_data = pd.DataFrame.from_items(records)
coord_data.to_csv(args.coord, index = False)