#!/usr/bin/env python3
import pyrat.geo.geofun as gf
import numpy as np
import itertools
import csv
import argparse as ap
import pyrat.fileutils.gpri_files as gpf
import matplotlib.pyplot as plt
parser = ap.ArgumentParser()
parser.add_argument("dem_seg_par", type=str)
parser.add_argument("inc", type=str, help="Incidence angle")
parser.add_argument("mask", type=str, help="Shadow mask [UCHAR]")
args = parser.parse_args()
inc = gpf.gammaDataset.fromfile(args.dem_seg_par, args.inc)

#Shadow mask
sh = inc > np.pi /2

# ly, sh = gf.ly_sh_map(inc, inc)
# plt.imshow(sh)
# plt.show()
gpf.write_binary(sh, args.mask, dtype=gpf.type_mapping['FLOAT'])