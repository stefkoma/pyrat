#!/usr/bin/python3
import argparse
from pyrat.gpri_utils import processors as _proc
from pyrat.fileutils import gpri_files as gpf
from pyrat.gpri_utils import calibration as cal

#Add command line parameters
parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('slc',
                    help="Uncorrected SLC file")
parser.add_argument('slc_par',
                    help="SLC parameters")
parser.add_argument('slc_out', type=str,
                    help="Output SLC")
parser.add_argument('slc_par_out', type=str,
                    help="Output SLC parameters")
parser.add_argument('--a', help="")
parser.add_argument('--r_ant', type=float, default=argparse.SUPPRESS,
                    help="Antenna rotation arm length. If not specified, it is calculated from the GPRI parameter file")
parser.add_argument('--r_ph',
                    help="Antenna phase center horizontal displacement",
                    type=float, default=0.15)
parser.add_argument('-w', '--win_size', dest='ws',
                    help="Convolution window size in degrees",
                    type=float, default=0.4)
parser.add_argument('-d', '--discard-samples', dest='discard_samples',
                    help="Discard samples after convolution",
                    action='store_true')
# Read arguments
try:
    args = parser.parse_args()
except:
    print(parser.print_help())
    exit(-1)

#Load SLC
slc = gpf.gammaDataset.fromfile(args.slc_par, args.slc)

#Create processors
corrected = cal.azimuth_correction(slc, args.r_ph, ws=args.ws, discard_samples=False)

#Save corrected 
corrected.tofile(args.slc_par_out, args.slc_out)
