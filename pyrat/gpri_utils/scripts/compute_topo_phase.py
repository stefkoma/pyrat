#!/usr/bin/env python3
import argparse
import pyrat.fileutils.gpri_files as gpf
import numpy as np
import matplotlib.pyplot as plt

import pyrat.core.corefun as cf
import pyrat.core.polfun as pf
import pyrat.visualization.visfun as vf

import scipy as sp
import scipy.spatial as spat

import pyrat.core.matrices as mat

import pyrat.gpri_utils.calibration as cal

import pyproj 
parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)


parser.add_argument("mli_par", type=str, help="reference mli parameters")
parser.add_argument("east",type=str, help="East coordinates")
parser.add_argument("north",type=str, help="North coordinates")
parser.add_argument("hgt",type=str, help="DEM height")
parser.add_argument("bl",type=float, help="Perpendicular baseline")
parser.add_argument("sim_topo",type=str, help="Simulated topographic phase")
args = parser.parse_args()

#Load positions
east, theta_par = gpf.load_dataset(args.mli_par, args.east)
north, theta_par = gpf.load_dataset(args.mli_par, args.north)
hgt, theta_par = gpf.load_dataset(args.mli_par, args.hgt)

#Compute radar position
#Load lat lon position from parameters
lv05 = pyproj.Proj(init='epsg:21781')
wgs84 = pyproj.Proj(init="epsg:4326")




radar_pos_wg84 = [theta_par['GPRI_ref_east'],theta_par['GPRI_ref_north'], theta_par['GPRI_ref_alt']+theta_par['GPRI_geoid']]
radar_pos_lv05 = np.array(pyproj.transform(wgs84, lv05, *radar_pos_wg84))

#Compute positions
r_vec = np.dstack((east,north,hgt)) - radar_pos_lv05[None,None,:]
r_vec_prime = r_vec + np.array([0,0,args.bl])[None,None,:]
#Compute difference in distance
dr = np.linalg.norm(r_vec_prime,axis=-1) - np.linalg.norm(r_vec,axis=-1) 
#Compute phase
lam = gpf.C / theta_par.radar_frequency 

topo_phase = 4 * np.pi / lam * dr
gpf.write_binary(topo_phase, args.sim_topo, dtype=gpf.FLOAT)