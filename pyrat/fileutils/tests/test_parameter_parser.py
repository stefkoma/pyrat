import unittest
import datetime as dt
from pyrat.fileutils.parsers import  FasterParser

heredoc="""
time_start:                              2016-09-14 14:51:07.652981 
geographic_coordinates:                  46.8602383333 7.5258866667 896.8 47.3 
RF_center_freq:                                17200000000.0 
RF_freq_min:                                   17100115853.7 
RF_freq_max:                                   17299884146.4 
RF_chirp_rate:                                 49942173063.8 
CHP_num_samp:                                          25000 
TX_mode:                                                None 
TX_RX_SEQ:                                              BBBl 
IMA_atten_dB:                                             44 
ADC_capture_time:                                   24.07118 
ADC_sample_rate:                                   6250000.0 
STP_antenna_start:                                     -45.0 
STP_antenna_end:                                        70.0 
STP_rotation_speed:                                      4.8 
STP_gear_ratio:                                           72 
TSC_acc_ramp_angle:                                      0.3 
TSC_acc_ramp_time:                                   0.09583 
TSC_acc_ramp_step:                                       0.1 
TSC_rotation_speed:                                  4.80076 
antenna_elevation:                                       0.0 
CHP_version:                                        SW X2.00 
TSC_version:                                        SW V3.04 
CHP_temperature:                                        31.8 
TSC_temperature:                                        29.3 
"""

class TestParser(unittest.TestCase):

    def setUp(self):
        self.n = 10
        self.array = range(1,self.n)
        self.units = ["m^{i}".format(i=i) for i in self.array]
        self.parser = FasterParser()

    def testIntParsing(self):
        hf = "array: {arr}".format(arr=' '.join(map(str, self.array)))
        parsed = self.parser.parse(hf)
        print(parsed.asDict())
        self.assertEqual(parsed.asDict()['array']['value'] , list(self.array) )

    def testDateParsing(self):
        date = dt.datetime(year=2016, month=6, day=14, hour=13, minute=12, second=23, microsecond=23)
        hf = """date: {dt}""".format(dt=date)
        parsed = self.parser.parse(hf)
        pd = parsed.asDict()
        print(parsed.pprint())
        self.assertEqual(pd['date'], date)

    def testSimpleParsing(self):
        hf ='a: 45'
        parsed = self.parser.parse(hf)

    def testTitleParsing(self):
        hf="""scewmo cuil\nr: 4\na: 5\n"""
        parsed = self.parser.parse(hf)
        print(parsed.asDict())

    def testMultilineTitle(self):
        hf="""This is\na long\ntitle \na:4"""
        parsed = self.parser.parse(hf)
        print(parsed.asDict())

    def testShortDateParsing(self):
        dt_str = dt.datetime(2015,8,3).date()
        hf="""date: {dt}\n""".format(dt=dt_str)
        parsed = self.parser.parse(hf)
        print(parsed.asDict())
        self.assertEqual(parsed.asDict()['date'], dt_str)

    def testUnitParsing(self):
        hf = """array: {arr} {units}
        """.format(arr=' '.join(map(str, self.array)), units=' '.join(map(str, self.units)))
        parsed = self.parser.parse(hf)
        print(parsed.asDict())
        self.assertEqual(parsed.asDict()['array']['unit'] , self.units )

    def testTimeStart(self):
        hf= """Boring titel\n

            time_start: 2016-11-04 16:43:26.512548+00:00\n
            """
        parsed = self.parser.parse(hf)
        print(parsed)

    def testRawParams(self):
        parsed = self.parser.parse(heredoc)
    