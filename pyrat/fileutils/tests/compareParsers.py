import datetime as dt
from pyrat.fileutils.parsers import  FasterParser
from pyrat.fileutils.parameters import ParameterFile
import timeit


heredoc="""
time_start:                              2016-09-14 14:51:07.652981 
geographic_coordinates:                  46.8602383333 7.5258866667 896.8 47.3 
ADC_capture_time:                                   24.07118
ADC_sample_rate:                                   6250000.0 
STP_antenna_start:                                     -45.0 
STP_antenna_end:                                        70.0 
STP_rotation_speed:                                      4.8 
STP_gear_ratio:                                           72 
TSC_acc_ramp_angle:                                      0.3 
TSC_acc_ramp_time:                                   0.09583 
TSC_acc_ramp_step:                                       0.1 
TSC_rotation_speed:                                  4.80076 
antenna_elevation:                                       0.0 
CHP_version:                                        SW X2.00 
TSC_version:                                        SW V3.04 
CHP_temperature:                                        31.8 
TSC_temperature:                                        29.3 
"""
with open("test.raw_par", 'w+') as of:

    of.write(heredoc)


fast = ParameterFile.from_file('test.raw_par', parser=FasterParser)
print(fast)
print(list(fast.items()))
# slow = ParameterFile.from_file('../default_slc_par.par', parser=ParameterParser)

