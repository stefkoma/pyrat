import csv

import numpy as _np

from ..fileutils import parameters as par
from ..fileutils.gpri_files import type_mapping as tm
from ..fileutils import  gpri_files as gpf

from ..geo import geofun as gf



def ptGPRI2geo(plist,ref_slc_par, gc_table):
    """
    Geocode a plist object of GPRI point scatterer
    locations with the 
    use of a geo.geofun.GeocodingTable object
    """
    #Determine range and azimuth looks
    rlks = gpf.get_width(ref_slc_par) / gpf.get_width(gc_table.mli_par)
    azlks = gpf.get_nlines(ref_slc_par) / gpf.get_nlines(gc_table.mli_par)
    pl_gc = [gc_table.dem_idx_to_radar_coord(gc_table.radar_idx_to_dem_idx([pos[0]//rlks, pos[1]//azlks])) 
    for pos in plist]
    pl_arr = _np.vstack(pl_gc).astype(gpf.FLOAT)
    pd = Pdata.fromarray(plist,pl_arr)
    return pd

#Format mapping to save values
format_mapping ={
'FCOMPLEX': '%1.3f',
'FLOAT': '%1.3f',
'REAL*4': '%1.3f',
'UCHAR': 'u'}



class Plist:
    """
    Class to represent a point target list
    """

    def __init__(self):
        self.plist = None
        self.params = None

    @classmethod
    def fromarray(cls, arr, params):
        instance = cls()
        instance.plist = list(arr)
        instance.params = params
        return instance

    @classmethod
    def fromfile(cls, plist_path, r_slc_par_path, **kwargs):
        instance = cls()
        plist = _np.fromfile(plist_path, dtype=gpf.INTEGER)
        instance.plist = list(plist.reshape((len(plist)//2, 2)))
        instance.params = par.ParameterFile.from_file(r_slc_par_path)
        return instance

    def __getitem__(self, item):
        # Deletage getitem to the numpy array
        return self.plist[item]

    def __getslice__(self, sl):
        return self.plist[sl][:]

    def __iter__(self):
        return self.plist.__iter__()

    def __len__(self):
        return self.plist.__len__()

    def closest_index(self, pos):
        """
        Find the point target index closest to
        the specified coordinate
        Parameters
        ----------
        pos

        Returns
        -------
        """
        plist_arr = _np.array(self.plist)
        residual = _np.sum((plist_arr- _np.array(pos)[None, :]) ** 2, axis=1)
        idx = _np.argmin(residual)
        return idx

    def radar_coords(self, pos):
        # Return the radar coordinates of the point of interest
        r = self.params.near_range_slc + pos[0] * self.params.range_pixel_spacing
        az = self.params.GPRI_az_start_angle + pos[1] * self.params.GPRI_az_angle_step
        return (r, az)

    def cartesian_coord(self, pos):
        (r, az) = self.radar_coords(pos)
        x = r * _np.cos(_np.deg2rad(az))
        y = r * _np.sin(_np.deg2rad(az))
        return (x, y)

    def to_location_list(self):
        res = []
        for idx_pt, pt in enumerate(self):
            coord = self.cartesian_coord(pt)
            res.append(coord)
        return res

    def pos_iter(self):
        for idx_pt, pt in enumerate(self):
            coord = self.cartesian_coord(pt)
            yield idx_pt, coord
    
    def tofile(self, fn):
        (_np.array(self.plist).astype(gpf.INTEGER)).tofile(fn)


class Pdata:
    def __init__(self):
        self.plist = None
        self.pdata = None


    def __getitem__(self, item):
        return self.pdata.__getitem__(item)

    def __getslice__(self, sl):
        return self.pdata.__getslice__(sl)

    @classmethod
    def fromarray(cls, plist, arr, dtype=gpf.FCOMPLEX):
        instance = cls()
        instance.plist = plist
        instance.pdata = _np.array(arr).astype(dtype)
        instance.dtype = dtype
        assert len(plist) == arr.shape[0]
        return instance

    @classmethod
    def fromfile(cls, plist_path, r_slc_par_path, pdata_path, dtype=gpf.FCOMPLEX):
        instance = cls()
        instance.plist = Plist.fromfile(plist_path, r_slc_par_path)
        pdata = _np.fromfile(pdata_path, dtype=dtype)
        nrecords = len(pdata) // len(instance.plist)
        pdata_shape = ( nrecords, len(instance.plist),)
        pdata = pdata.reshape(pdata_shape).T
        instance.pdata = pdata
        return instance

    @property
    def npt(self):
        return self.pdata.shape[0]

    @property
    def nrecords(self):
        return self.pdata.shape[1]

    @property
    def ridx(self):
        return [x[0] for x in self.plist]

    @property
    def azidx(self):
        return [x[1] for x in self.plist]

    def to_location_list(self):
        res = []
        for idx_pt, pt in enumerate(self.plist):
            val = self.pdata[:, idx_pt]
            idx = self.plist[idx_pt]
            res.append(list(idx) + list(val))
        return res

    def to_csv(self, of, take=None, delimiter=' ', header=False):

        #Datatypes
        dtypes = [('ridx', 'i'), ('azidx', 'i')] + [
            ('record_{n}'.format(n=n), self.pdata.dtype) for n in range(self.nrecords)]
        #Get format
        fmt = ['%i', '%i'] + [format_mapping[gpf.numpy_dt_to_gamma_dt(self.pdata.dtype)],] * self.nrecords
        fmt_str = " ".join(fmt)
        headers = [dt[0] for dt in dtypes]
        rec = _np.hstack([self.plist, self.pdata])
        if header:
            headerTxt = " ".join(headers),
        else:
            headerTxt = ""
        _np.savetxt(of, rec, fmt=fmt_str, header=headerTxt, comments="", delimiter=delimiter)


    def tofile(self, fn):
        (self.pdata.astype(self.pdata.dtype)).T.tofile(fn)
