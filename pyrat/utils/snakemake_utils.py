from snakemake.io import Params


class DefaultGammaParams(Params):

    def __getitem__(self, key):
        try:
            return super().__getitem__(key)
        except:
            return "-"
    
    def __getattr__(self, key):
        try:
            return super.__getattr__(key)
        except:
            return "-"