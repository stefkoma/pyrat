from unittest import TestCase

import numpy as np
from .. import corefun as cf


def test_vectorize_array():
    A = np.random.randn(10,2,3,5)
    A_vec = cf.vectorize_matrix(A)
    assert A_vec.shape[0:2] == A.shape[0:2]