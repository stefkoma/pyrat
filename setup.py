import setuptools
from setuptools import setup

# Find all scripts
scripts = setuptools.findall('pyrat/gpri_utils/scripts/')
setup(name='pyrat',
      version='0.1',
      description='Python radar tools',
      author='Marcel Stefko',
      author_email='stefko@ifu.baug.ethz.ch',
      license='MIT',
      packages=setuptools.find_packages(exclude=['*tests*']),
      zip_safe=False,
      install_requires=['numpy', 'matplotlib>=2.0', 'pyparsing', 'pyfftw', 'gdal', 'pillow', 'scikit-image', 'cartopy', 'pandas', 'nose', 'pyproj'],
      package_data={'default_slc_params': 'fileutils/default_slc_par.par',
                    'default_prf': 'fileutils/default_prf.prf', 'list_of_slcs':['diff/tests/list_of_slcs.csv'],
                    },
      scripts=scripts,
      include_package_data=True)
